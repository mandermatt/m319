# Input: Operatoren

Operatoren werden in C# - wie in allen anderen Programmiersprachen - für unterschiedlichste Aufgaben verwendet. Dazu gehört neben dem Ausführen von **mathematischen Berechnungen** auch das **Vergleichen** und **Zuweisen** von Werten, die **logische Verknüpfung** von boolschen Werten bzw. Aussagen oder auch die Ausführung von binären Rechenoperationen.

In C# ist es sogar möglich, die Funktionalität von bestehenden Operatoren zu *verändern* (siehe: [Operator Overloading](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/operator-overloading)).

## Operatorenpräzedenz

Betrachten Sie den folgenden mathematischen Ausdruck: $`x = 6 + 4 \cdot 5 : 2`$. Was ist das Ergebnis für $`x`$? 25? 13? **16!**

Die mathematische Rechenvorschrift *Punkt vor Strich* ist Ihnen seit der Primarschule bekannt. Diese Regel besagt, dass "Punkt-Operationen" (Multiplikation und Division) vor "Strich-Operationen" (Addition und Subtraktion) ausgeführt werden. Die gleichen Regeln gelten natürlich auch bei der Anwendung von Operatoren in einer Programmiersprache. Zusätzlich müssen in der Programmierung noch zahlreiche weitere Operatoren bzw. deren *Reihenfolge bei der Berechnung*, die sogenannte *Operatorenpräzedenz*, berücksichtigt werden.

## Operatorassoziativität

Es gibt Operatoren, welche die gleiche Präzedenz haben. Für diese Operatoren ist festgelegt, in welcher Reihenfolge die Auswertung durchgeführt wird.

Als Beispiel soll wiederum ein mathematischer Ausdruck betrachtet werden: $`64 : 8 : 4 : 2`$. Das Ergebnis dieser Rechnung ist unterschiedlich für eine *linksassoziative* oder *rechtsassoziative* Division. Das korrekte Ergebnis beträgt 1, weil die Berechnung "von links nach rechts" erfolgt. Eine Schreibweise mit Klammern ist unnötig, weil folgende Reihenfolge der Divisionen durch die linksassoziativität des Divisionsoperators gegeben ist: $`(((64 : 8) : 4) : 2)`$. Bei einer allfälligen *rechtsassoziativen* Definition der Division würde folgender Ausdruck mit dem Ergebnis 16 resultieren: $`(64 : (8 : (4 : 2))) = 16`$.

In C# sind fast alle Operatoren *linksassoziativ*. Ausgenommen von dieser Regel sind die *Zuweisungsopoeratoren* sowie der *NULL-Sammeloperator*.

## Anzahl der Operanden

Die Operatoren können hinsichtlich Ihrer Operandenanzahl in *unäre*, *binäre* und *ternäre* Operatoren mit einem bzw. zwei bzw. drei Operanden unterschieden werden. Höherwertige Operatoren mit mehr als drei Operanden existieren (aktuell) in C# keine.

## Liste der Operatoren (unvollständig)

In der nachfolgenden Tabelle werden einige relevante Operatoren in C# mit Ihrer Bedeutung sowie einem Code-Beispiel aufgeführt. Von oben nach unten gelesen wiederspiegelt die Auflistung die tatsächliche Operatorenpräzedenz.

| Bezeichnung | Operator(en) | Beispiele |
| ----------- | ------------ | --------- |
| Unäre Operatoren | `+`, `-`, `!`, `++`, `--` | `+6`, `-18`, `!someBoolVariable`, `++x`, `--y`|
| Arithmetische Operatoren | `*`, `/`, `%`| `x * 2`, `15 / y`, `18 % 4` |
| Arithmetische Operatoren | `+`, `-` | `x + 2`, `15 - y` |
| Vergleichsoperatoren | `<`, `>`, `<=`, `>=` | `x < 5` `y > 42`, `p <= 16`, `q >= 35` |
| Vergleichsoperatoren | `==`, `!=` | `x == 5` `y != 42` |
| Bedingte Logische Operatoren | `&&` | `someBoolExpression && otherBoolExpression` |
| Bedingte Logische Operatoren | `||` | `someBoolExpression && otherBoolExpression` |
| Bedingter Operator | `?:` | `a > 5 ? true : false` |
| Zuweisungsoperatoren | `=`, `*=`, `/=`, `%=`, `+=`, `-=` | `a = 7`, `b *= 2`, `c /= -6`, `d %= 5`, `e += 15`, `f -= 4 `|

Eine [vollständige Liste](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/) aller in C# verfügbaren Operatoren ist in der Sprachreferenz einsehbar.

## Beispiel-Applikation

Das nachfolgende Code-Beispiel zeigt einige der oben genannten Operatoren in einem grösseren Kontext. Dabei werden zur besseren Verdeutlichung neben den eigentlichen Operatoren auch andere Sprachkonstrukte wie *Bedingungen* (Selektion) und *Schleifen* (Iteration) eingesetzt.

```c#
// Declare a variable of type integer.
int a;

// Use the = assignment operator to assign a value to variable a.
a = 12;

// Increment the value of a by 1.
a++;

// Assign the remainder of the integer devision to another variable (b).
int b = a % 5;  // b = 3

// Check if b is greater or equal than 3.
if (b >= 3)
{
    // Assign the result of the addition a+5 to the variable a.
    a += 5; // a = 18
}

// Assign the product of negative a times b to the variable b
b *= -a;    // b = -54

// Check if a AND b are greater than zero.
if (a > 0 && b > 0)
{
    // No code here, since the result of the above condition is wrong.
    // Although a is greater than 0, the whole condition evaluates to false
    // since c is -54 and therefore less than 0.
}

// Starting with c=54, the statements inside the loop are repeated as long
// as the value of c is greater or equal than 0. After each execution, using
// the decrement operator, the value of c is reduced by 1.
for (int c = -a; c >= 0; c--)
{
    // Check if the value of c is divisible by 3 AND 5 without remainder.
    if (c % 3 == 0 && c % 5 == 0)
    {
        System.Console.WriteLine(c + " is divisible by 3 and 5.")
    }
}
```