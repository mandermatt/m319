# Input: Input und Output

In der Softwareentwicklung werden nur ganz selten Programme geschrieben, welche gänzlich ohne die Eingabe und Ausgabe von Daten funktionieren. Letztlich führen grundsätzlich immer *unterschiedliche Daten* dazu, dass ein Programm *unterschiedlich abläuft*. Oder umgekehrt formuliert: Ohne Veränderung bei den zu verarbeitenden Daten wird grundsätzlich immer das gleiche Ergebnis erwartet. Gemäss [Wikipedia](https://de.wikipedia.org/wiki/Algorithmus#Eigenschaften_des_Algorithmus) ist genau dieser Umstand eine wesentliche Eigenschaft jedes Algorithmus'.

## Ein- und Ausgabe mit C#

Die Eingabe und Ausgabe von Daten lässt sich mit der Programmiersprache C# grundsätzlich auf ganz unterschiedliche Arten umsetzen. Als mögliche Datenquellen können beispielsweise Textdateien mit strukturierten Daten (`.csv`, `.json`, `.xml`, ...), Dateien mit binären Daten, Datenbanken, Schnittstellen anderer Applikationen oder Eingaben von Benutzerinnen und Benutzern genutzt werden. Alle diese unterschiedlichen Datenquellen können analog auch als mögliche Ziele für die Ausgabe bzw. Speicherung von Daten genutzt werden.

Die Einbindung von Datenbanken und die Kommunikation über Schnittstellen erfordert fortgeschrittene Programmierkenntnisse in unterschiedlichen Teilbereichen der Softwareentwicklung. Auch die Verarbeitung von Dateien ist für Anfängerinnen und Anfänger nicht trivial. Zumindest müssten viele wesentliche Aspekte bei der Verwendung dieser Input- und Outputmöglichkeiten vereinfacht oder ignoriert werden. Aus diesem Grund wird im Modul 319 vorerst auf deren Verwendung verzichtet und stattdessen eine einfachere Dateneingabe und -ausgabe über die Konsole genutzt.

### Daten auf die Konsole schreiben

Für die Ausgabe von Daten auf die Konsole existieren in C# zwei verschiedene, jedoch grundsätzlich ähnliche Möglichkeiten:
- `Console.Write` zur Ausgbe von Text ohne automatischen Zeilenumbruch
- `Console.WriteLine` zur Ausgabe von Text wobei am Ende automatisch ein Zeilenumbruch hinzugefügt wird

 Die beiden genannten *Methoden*[^1] befinden sich im Namensraum (englisch: *namespace*) `System`. Damit eine dieser Methoden aufgerufen werden kann, muss entweder der Namensraum mit einer `using`-Direktive hinzugefügt werden, oder der der Methodenaufruf mit vorangestelltem Namensraum erfolgen. Bei der zweiten Option erfolgt der Methodenaufruf über den sogenannten *fully qualified name*.

```c#
// Add namespace "System" for current file
using System;

// Since the namespace is imported:
// No need to call the method with its fully qualified name.
Console.WriteLine("Method is called with its relative name");

// Call methods with fully qualified name. This is only necessary
// if there's no using statement for the "System" namespace.
System.Console.WriteLine("Methid is called with its fully qualified name");
```

#### Dynamische Datenausgabe

Im obenstehenden Code-Beispiel werden Daten auf die Konsole geschrieben, welche bereits zur Kompilierzeit - als zum Zeitpunkt der Programmierung - bekannt sind. Häufig sollen jedoch auch Daten ausgegeben werden, welche zur Laufzeit - also unmittelbar bei der Ausführung des Programms - bestimmt werden. Beispielsweise durch die Auswertung von Eingaben oder aufgrund anderer aktueller Gegebenheiten (beispielsweise aufgrund der jeweils aktuellen Uhrzeit). In den nachfolgenden Code-Beispielen werden verschiedene Möglichkeiten zur Ausgabe solcher "dynamischer Daten" illustriert.

```c#
using System;

DateTime now = DateTime.Now;

// String concatenation with + operator
Console.WriteLine("Current time: " + now.Hour + ":" + now.Minute);

// String interpolation with $ prefix
Console.WriteLine($$"Current date: {now.Day}.{now.Month}.{now.Year}");
```

#### Escapesequenzen

Mit den Methoden `Console.Write` bzw. `Console.WriteLine` können grundsätzlich nur Werte des Datentyps `string` auf die Konsole geschrieben werden. Zur Begrenzung von Strings müssen die doppelten Anführungszeichen `"` (manchmal auch als *Gänsefüsschen* bezeichnet) verwendet werden. Folglich ist die Verwendung dieses Zeichens (`"`) in einem String nicht ohne weiteres möglich - weil sonst ja der String "geschlossen" bzw. "beendet" würde.

Die Lösung für dieses Problem besteht im sogenannten *Escape Sequences*. Mit dem *Backslash* (`\`) können verschiedene Sonder- und Steuerungszeichen in einem String codiert werden. Nachfolgend werden einige gebräuchliche *Escaping Sequences* aufgeführt. Eine [vollständige Liste](https://docs.microsoft.com/en-us/cpp/c-language/escape-sequences?view=msvc-160#escape-sequences-1) ist in der C#-Sprachreferenz verfügbar.

| Escapesequenz | Bedeutung |
| ------------- | --------- |
| `\n` | Zeilenumbruch |
| `\t` | Horizontaler Tabulator |
| `\"` | Doppeltes Anführungszeichen |
| `\'` | Einfaches Anführungszeichen |
| `\\` | Backslash |


### Daten von der Konsole einlesen

Um Daten von der Konsole einzulesen können für verschiedene Anwendungsfälle unterschiedliche Methoden verwendet werden. Auch die Methoden für das Einlesen von Eingabedaten befinden sich im Namensraum `System`. Für die Verwendung der nachfolgend genannten Methoden muss dieser Namensraum folglich mittels `using`-Direktive importiert oder dem Methodennamen vorangestellt werden.

- `Console.ReadLine` liest eine Zeile von Zeichen ein und gibt diese als `string` zurück.
- `Console.Read` liest ein einzelnes Zeichen ein und gibt dessen "Zahlenwert" als `int` zurück.
- `Console.ReadKey` liest ein einzelnes "Zeichen" (bzw. eine *Taste*) ein und gibt ein Objekt der Klasse `ConsoleKeyInfo` zurück. Die Verwendung dieser Methode ist (aktuell) meist nur dann sinnvoll, wenn auf die Eingabe eier spezifischen Taste reagiert werden soll.

Die Verwendung der oben genannten Methoden wird im nachfolgenden Codebeispiel illustriert.

```c#
using System;

// Read a whole line of text.
// The execution of the program continues when the user presses the ENTER key.
// The result is stored in the string variable.
Console.Write("Please enter your name: ");
string lineOfText = Console.ReadLine();
Console.WriteLine($"You entered: {lineOfText}\n");

// Reads continuously a single character.
// The execution of the program continues as soon as the user presses any key.
// The result is stored in the integer variable.
Console.Write("Please enter your name: ");
int characterCode;
do {
    characterCode = Console.Read();
    if (characterCode!=10) {
        Console.Write($"You entered: {Convert.ToChar(characterCode)}");
        Console.WriteLine($" [Code: {characterCode}]");
    }
} while (characterCode != 10);  // Press ENTER to leave the loop

// Read a single key.
// The execution of the program continues as soon as the user presses any key.
// The result is stored (as an object) in the variable of type ConsoleKeyInfo.
Console.Write("\nPlease press any key on your keyboard: ");
ConsoleKeyInfo key = Console.ReadKey();
Console.WriteLine($"\nYou pressed: {key.Key}");
```

#### Ganze Zahlen einlesen

Um numerische Berechnungen in einer Applikation ausführen zu können, müssen die eingelesenen Daten zuerst konvertiert werden. Um mehrstellige Zahlen einzulesen wird sinnvollerweise die Methode `Console.ReadLine` verwendet. Diese gibt jedoch ausschliesslich Werte des Datentyp `string` zurück - auch wenn die Eingabedaten rein numerisch sind.

Für die Umwandung von "numerischen Strings" in Daten des Datentyps `int` existieren mehrere verschiedene Varianten. Die beiden nachfolgenden Methoden decken einen Grossteil aller Anwendungsfälle im Modul 319 ab:

- `Convert.ToInt32` um einen "numerischen String" in eine Ganzzahl (`int`) umzuwandeln. Der Rückgabewert entspricht (bei einem "gültigen" String) dem ganzzahligen Wert.
- `int.TryParse` um einen String in eine Ganzzahl (`int`) umzuwandeln. Der Rückgabewert ist vom Typ `bool` und gibt an, ob die Konvertierung erfolgreich war. Im Erfolgsfall, sofern der Rückgabewert `true` ist, wird der ganzzahlige Wert im zweiten Parameter gespeichert.

Die Funktionsweise der oben genannten Methoden wird im nachfolgenden Codebeispiel illustriert.

```c#
Console.WriteLine("Please enter an integer:");
string input = Console.ReadLine();

int numericValue;

if (int.TryParse(input, out numericValue))
{
    // Input was successfully converted to int.
    Console.WriteLine($"Your number is greater than {numericValue - 1} by one.");
}
else
{
    System.Console.WriteLine("Conversion of input to integer failed!");
}

// This will fail (result in an exception) if the conversion is not possible.
numericValue = Convert.ToInt32(input);
if (numericValue % 2 == 0)
{
    Console.WriteLine($"Your number is EVEN");
}
else
{
    Console.WriteLine($"Your number is ODD");
}
```

#### Fliesskommazahlen einlesen

Die Umwandlung von "numerischen Strings" in Fliesskommazahlen funktioniert analog zur Umwandlung in ganze Zahlen. Statt des Datentyps `int` für ganze Zahlen wird dazu entweder `float` oder `double` verwendet. Mögliche Methodenaufrufe lauten dann beispielsweise `float.TryParse` oder `Convert.ToDouble`.

#### Daten einlesen in Visual Studio Code

Um mit den oben gezeigten Methoden Eingabedaten in Visual Studio Code einzulesen, muss das Programmierprojekt entsprechend vorbereitet sein. Dazu müssen gemäss der Anleitung im Dokument [00 Organisatorisches/Technisches.md](../00%20Organisatorisches/Technisches.md#dateneingaben-verarbeiten) (Abschnitt: *Dateneingaben verarbeiten*) in der Datei `.vscode/launch.json` jeweils eine Zeile angepasst bzw. ergänzt werden:

```json
{
    "configurations": [
        {
            // other configuration entries...

            "console": "integratedTerminal",
            "internalConsoleOptions": "neverOpen",

            // other configuration entries...
        }
    ]
}
```


[^1] Methoden sind funktionale Einheiten in einem objektorientierten Kontext. Die Definition und die Verwendung von Methoden sind ein zentraler Bestandteil des Modul 320.