# `if-else`-Selektion

Mit einer `if-else`-Selektion können Anweisungen in Abhängigkeit einer Bedingung ausgeführt werden. Wenn die Bedingung erfüllt ist, d.h. die Auswertung der Bedingung den boolschen Wert `true` ergibt, werden die auf das Schlüsselwort `if` folgenden Anweisungen ausgeführt. Wenn die Auswertung der Bedingung den boolschen Wert `false` ergibt, werden die Anweisungen, welche auf das Schlüsselwort `else` folgen, ausgeführt. Dabei werden jeweils nur die einen *oder* anderen Anweisungen ausgeführt - es gibt keine Möglichkeit bei einer `if-else`-Selektion, beide Anweisungsblöcke ausführen zu lassen.

```c#
if (Bedingung)
{
    Anweisungen IF
}
else
{
    Anweisungen ELSE
}
```

Die *Bedingung* der `if-else`-Selektion wird nach dem `if` Schlüsselwort in runden Klammern geschrieben. Dabei kann diese Bedingung beispielsweise eine Variable vom Datentyp `bool` sein. Häufiger werden jedoch Ausdrücke verwendet, welche als Ergebnis einen boolschen Wahrheitswert (`true` oder `false`) haben. Meist werden dazu Vergleichsoperatoren wie `>`, `<`, `>=`, `<=`, `==` oder `!=` verwendet.

Die selektiv auszuführenden Anweisungen bei positivem Ergebnis der Bedingung (`true`) werden nach der Bedingung zwischen geschweifte Klammern `{` und `}` geschrieben. Sie selektiv auszuführenden Anweisungen bei negativem Ergebnis der Bedingung (`false`) werden nach dem Schlüsselwort `else` zwischen geschweifte Klammern `{` und `}` geschrieben. Wenn der selektiv auszuführende Code-Block aus lediglich einer Anweisung besteht, könnte diese Anweisung auch direkt nach der Bedingung bzw. dem Schlüsselwort `else`, ohne geschweifte Klammern, geschrieben werden. Von dieser Notation wird jedoch abgeraten, da sie gerade bei Programmieranfängerinnen und -anfängern schneller zu unübersichtlichen Code-Stellen oder Fehlern führen kann.

```c#
// Declare a variable to save the number of days in a month.
int days;

// Check for february - it has only 28 days.
if (month == 2)
{
    days = 28;
}
// Every other month has more than 28 days.
else
{
    // From january to july...
    if (month <= 7)
    {
        // ... even months have 30 days.
        if (month % 2 == 0)
        {
	        days = 30;
        }
        // ... odd months have 31 days.
        else {
            days = 31;
        }
    }
    
    // Starting from august...
    else
    {
        // ... even months have 31 days.
        if (month % 2 == 0)
        {
            days = 31;
        }
        // ... odd months have 30 days.
        else
        {
            days = 30;
        }
    }
}
```
