# `for`-Iteration

Bei der `for`-Iteration werden die Anzahl der Wiederholungen für einen Anweisungsblock durch drei verschiedene Komponenten im Schleifenkopf definiert. Die *Initialisierung*, *Bedingung* und *Iteration* werden dazu jeweils getrennt durch ein Semikolon, nach dem Schlüsselwort `for`, in runden Klammern notiert:

- Der erste Teil ist die *Initialisierung*. In diesem Teil wird der Anfangswert einer Zählvariable festgelegt. Die Initialisierung wird genau ein Mal, vor der ersten Bedingungsprüfung ausgeführt.
- Der zweite Teil ist die *Bedingung*. Die Anweisungen im Schleifenkörper werden so lange ausgeführt, wie die Bedingung *wahr* ist. Die Bedingung wird fortlaufend, jeweils *vor* einem Schleifendurchlauf, geprüft.
- Der dritte Teil ist der *Iterator*. In diesem Teil wird definiert, wie der Wert der Zählvariable nach einem Schleifendurchlauf verändert wird. Der Iterator wird fortlaufend, jeweils *nach* einem Schleifendurchlauf, ausgeführt.

```c#
for (Initialisierung; Bedingung; Iterator)
{
    Anweisungen
}
```

Typischerweise wird für die Zählvariable eine Variable von Datentyp `int` (integer, Ganzzahl) verwendet. Dabei wird der Wert, je nach Kontext, im Iterator erhöht oder verringert. Die Schrittweite dieser Veränderung ist meistens `1`, kann jedoch beliebig gewählt werden.

Bei der Verwendung der `for`-Schleife sollte - im Gegensatz zur `while` oder `do-while` Schleife - der Wert der Zählvariable (jene Variable, welche für die Formulierung der Bedingung verwendet wird) im Anweisungsblock der Schleife *nicht* verändert werden. Die einzige Manipulation der Zählvariable sollte im *Iterator* der `for`-Schleife erfolgen.

```c#
// This is necessary for the Console.WriteLine statement
// inside the if-condition of the for-loop.
using System;

// The counting variable is initialized with the value of 3.
// The loop is executed as long the value of i is lower than 100.
// After each execution, the value of i is increased by 3.
for (int i = 3; i < 100; i += 3)
{
    // Check if the value of i is divisible by 4.
	if (i % 4 == 0)
	{
		Console.WriteLine($"{i} is divisible by 3 and 4.");
	}
}
```
